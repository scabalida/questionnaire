<?php
require('login.php'); 

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Questionnaire</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">

      <form class="form-signin" action="" method="post">
        <h3 class="form-signin-heading" style="text-align:center;">Questionnaire App</h3>
        <label for="inputUser" class="sr-only">Username</label>
        <input type="text" name="username" id="inputEmail" class="form-control" placeholder="Username" required="" autofocus="">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password"name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
		<span style="padding-left:25px;color:red;"><?php echo $error; ?></span>
        <input class="btn btn-lg btn-primary btn-block" name="submit" type="submit" value="Sign In">
      </form>

    </div> 

</body></html>