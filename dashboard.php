<?php
require('getInstructors.php'); 

?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin Dashboard</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">

  </head>

  <body>
		<!-- Add Instructor Modal -->
	<div id="addInstructorModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Add Instructor</h4>
		  </div>
		  <div class="modal-body">
			<form class="form-signin" action="addInstructor.php" method="post" role="form">
				<input type="text" name="username" class="form-control" placeholder="Username" required>
				<br>
				<input type="password" name="password" class="form-control" placeholder="Password" required>
				<br>
				<input type="password" name="repassword" class="form-control" placeholder="Confirm Password" required>
				<br>
				<input type="text" name="firstname" class="form-control" placeholder="First Name" required>
				 <br>
				<input type="text"name="middlename" class="form-control" placeholder="Middle Name" required>
				 <br>
				<input type="text"name="lastname" class="form-control" placeholder="Last Name" required>
		 
		  </div>
		  <div class="modal-footer">
			<input type="submit" value="Save" name="submit" class="btn btn-primary" >
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			 </form>
		  </div>
		</div>

		</div>
	</div>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		   <a class="navbar-brand" href="#">Questionnaire App</a>
		</div>
         
		  <div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="logout.php"><i class="glyphicon glyphicon-off"></i> Log Out</a></li>
			  </ul>
			</div>
        
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="dashboard.php">Instructors <span class="sr-only">(current)</span></a></li>
            <li><a href="subjects.php">Subjects</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">List of Instructors</h1>
		 
          <div class="table-responsive">
		   <a class="btn btn-success pull-right" data-toggle="modal" data-target="#addInstructorModal">Add Instructor</a>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Middle Name</th>
                  <th>Last Name</th>
                </tr>
              </thead>
              <tbody>
				<?php if(count($data) == 0){?>
				<tr>
					<td colspan="3">No available data.</td>
				</tr>
				<?php } else{ ?>
				<?php foreach($data as $new_row){?>
                <tr style="color:#337ab7;">
                  <td><?php echo ucwords($new_row['firstname']);?></td>
                  <td><?php echo ucwords($new_row['middlename']);?></td>
                  <td><?php echo ucwords($new_row['lastname']);?></td>
                </tr>
				<?php } }?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <script src="js/jquery.min.js."></script>
    <script src="js/bootstrap.min.js"></script>
  

</body></html>